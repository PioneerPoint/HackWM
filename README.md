# HackWM
A super modular and hackable window manager written in Python. Formerly [ZeroWM](https:/gitlab.com/dylanhamer/ZeroWM).

### Note: I am currently refactoring this entire project.

## Usage

A debug script is provided with the project [here](HackWM-Debug.sh), run this with `./HackWM-Debug.sh` to spawn HackWM in a Xephyr window.
To use HackWM as a main window manager (which is not recommended) add the relevant commands to your `.Xinitrc`.

## The module system

HackWM is made up of modules that are imported by the main Core.py file. These are found in the [modules directory.](https://gitlab.com/DylanHamer/HackWM/tree/master/Modules)
Each module has a specific purpose and can be modified or replaced to augment or remove functionality. This makes HackWM very customizable and easy to fit a specific use case.

Current modules include:

- Keyboard (Handle key pressed and key bindings)
- Mouse (Handle mouse movements)
- Wallpaper (Dummy wallpaper setter, simply invokes feh to set a root window)
- Border (Draws window borders)

## Wallpapers

Wallpapers are found in the [wallpapers directory](https://gitlab.com/DylanHamer/HackWM/tree/master/Wallpapers) by default. Out of the box, this only contains a single built in wallpaper. To get more, clone [this repo.](https://gitlab.com/DylanHamer/HackWM-Wallpapers)

## Config

I'm currently developing the config system so there's not much to say about it. However, it is formatted using JSON and can be found in the [config directory](Config).

## Screenshots

![Screenshot of HackWM](Screenshots/DemoScreenshot.png)
Note: This screenshot is from ZeroWM which is a very early build of HackWM.

![Screenshot of HackWM]()
Note: This is a very early build of HackWM, the current version may differ in default appearence and source code.

![Screenshot of HackWM]()
Note: This is running with extra programs (Polybar and urxvt) which must be installed to achieve this appearence.

## FAQ

- Q: Does this use Wayland?
  A: No, it uses the Python X bindings via Python-Xlib
  
- Q: Have you thought about using Wayland?
  A: I currently have no plans to support Wayland for HackWM. I am only familiar with the X API and all my test systems use X.
    
- Q: How do I run this?
  A: See the `Usage` section above. 
      
- Q: How did you get your prompt like that in the screenshot?
  A: [Oh-My-ZSH](https://github.com/robbyrussell/oh-my-zsh) with Agnoster

- Q: Do you need help with this project?
  A: Currently, no. I'm currently working on a big refactor. And at the moment, only your deity of choice and I know how everything goes together.

## Known bugs

- Sometimes some garbage is spit out during exit.
 
## Future plans

- Add mouse cursor graphics.
- Expand wallpaper setter into fully featured wallpaper editor.
- Make tiling stable.
- Develop debug tools (key debugger, etc).
- I'm currently working on the ability to 'draw' windows, where you can use your mouse cursor to a drag an area and a window will spawn there with the correct dimensions.

## Thanks to:

[Eberhard Grossgasteiger](https://www.pexels.com/u/eberhardgross/) on Pexels for the default wallpaper.



