"""
Log
Simple logging function
By Kat Hamer
"""

import time  # Get time of logs
import click  # Color support

logFormat = "{level}: {message}"  # Format of log

def log(level, message, accent="blue"):
    timeString = time.strftime("%H:%M:%S")  # Current time string
    logMessage = logFormat.format(time=timeString, level=click.style(level, fg=accent), message=message)
    print(logMessage)
