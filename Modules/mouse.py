"""
Mouse
Window manager mouse handler
By Kat Hamer
"""

from Modules import log

from Xlib import X  # Main X module

class mouse:
    def __init__(self, config, wm):
        self.dragWindow = None
        self.sizeWindow = None
        self.mode = None
        self.modifier = config["keyboard"]["modifier"]
        self.wm = wm
        self.leftClick = 1
        self.rightClick = 3

    def configureMouse(self, window):
        """Grab mouse events for a window"""
        window.grab_button(0, X.Mod1Mask, True,
                           X.ButtonMotionMask | X.ButtonReleaseMask | X.ButtonPressMask,
                           X.GrabModeAsync,
                           X.GrabModeAsync,
                           X.NONE,
                           X.NONE,
                           None)

    def handleMouseMove(self, event):
        """Handle a mouse press"""
        if event.window not in self.wm.lockedWindows:  # Don't do anything if window is locked
            if self.mode == "move":
                self.moveWindow(event)
            elif self.mode == "resize":
                self.resizeWindow(event)

    def moveWindow(self, event):
        """Move a window"""
        if self.dragWindow is None:
            log.log("mouse", "starting window move")
            self.dragWindow = event.window
            windowGeometry = self.dragWindow.get_geometry()
            self.moveX = windowGeometry.x - event.root_x
            self.moveY = windowGeometry.y - event.root_y
        else:
           # log.log("mouse", "continuing move")
            self.dragWindow.configure(x = self.moveX + event.root_x,
                                      y = self.moveY + event.root_y)

    def resizeWindow(self, event):
        """Move a window"""
        if self.sizeWindow is None:
            log.log("mouse", "starting window resize")
            self.sizeWindow = event.window

            windowGeometry = self.sizeWindow.get_geometry()
            self.moveW = windowGeometry.width - event.root_x
            self.moveH = windowGeometry.height - event.root_y
        else:
            #log.log("mouse", "continuing resize")
            self.sizeWindow.configure(width = self.moveW + event.root_x,
                                      height = self.moveH + event.root_y)

    def handleMousePress(self, event):
        if event.detail == self.leftClick:
            self.mode = "move"
        elif event.detail == self.rightClick:
            self.mode = "resize"

        event.window.configure(stack_mode=X.Above)

    def handleMouseRelease(self, event):
        self.dragWindow = None
        self.sizeWindow = None
        self.mode = None
