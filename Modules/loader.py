"""
Loader
Simple information script for core
By Kat Hamer
"""

import click  # Colour support

accentColor = "blue"  # Accent colour
logo = """
 _   _            _   __        ____  __
| | | | __ _  ___| | _\ \      / /  \/  |
| |_| |/ _` |/ __| |/ /\ \ /\ / /| |\/| |
|  _  | (_| | (__|   <  \ V  V / | |  | |
|_| |_|\__,_|\___|_|\_\  \_/\_/  |_|  |_|
"""

def loader():
    """Show loader"""
    click.clear()
    click.secho(logo, fg=accentColor)
    click.echo()
    click.secho("HackWM, ", fg=accentColor, nl=False)
    click.secho("a super modular and hackable window manager written in Python.")
    click.secho("Coded with ", nl=False)
    click.secho("<3 ", fg="red", nl=False)
    click.secho("by Kat Hamer.")
    click.echo()
    click.secho("Licensed under the MIT license.")
    click.secho("Source code: https://gitlab.com/DylanHamer/HackWM", fg="red")
    click.echo()
