"""
Hooks
User configurable hooks file
By Kat Hamer
"""


class hooks(object):

    def __init__(self, wm):
        self.wm = wm

    def newWindow(self):
        """Called when a new window is created"""
        pass

    def destroyWindow(self):
        """Called when a window is closed"""
        pass

    def eventHandled(self):
        """Called when an event is handled"""
        pass

    def startup(self):
        """Run when window manager is started"""
        pass

    def loop(self):
        """Run each time the window manager is looped"""
        pass
