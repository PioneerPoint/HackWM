#!/usr/bin/python3

"""
HackWM - Core
HackWM core - based on CactusWM core
By Kat Hamer
"""

"""Xlib imports"""
from Xlib.display import Display  # Xlib display class
from Xlib import X  # Main X module

"""HackWM imports"""
from Modules import loader     # Greeter message
from Modules import keyboard   # Keyboard handler
from Modules import log        # Logger
from Modules import wallpaper  # Wallpaper setter
from Modules import mouse      # Mouse handler
from Modules import border     # Handles window borders
from Modules import hooks      # User hooks
from Modules import config     # Config loader

"""Other imports"""
from subprocess import Popen  # Spawn processes
from time import sleep  # Free up CPU during event loop

class hackwm:
    """Main window manager class"""

    def __init__(self):
        """Initialise core functionality"""
        self.focusedWindow = None  # Currently focused window
        self.windowList = []  # List of open windows
        self.lockedWindows = []  # List of windows that cannot be moved or resized
        self.hiddenWindow = None  # Currently hidden window

        self.configureDisplay()  # Configure display
        self.setConfigValues()  # Load global variables from config
        self.configureDispatchers()  # Configure event dispatchers

    def configureDisplay(self):
        """Configure display and root window"""
        self.display = Display()  # Initialise display
        self.rootWindow = self.display.screen().root  # Initialise root $
        self.rootWindow.change_attributes(event_mask=X.SubstructureRedirectMask)  # Get events from root window

        self.displayWidth = self.rootWindow.get_geometry().width
        self.displayHeight = self.rootWindow.get_geometry().height

    def setConfigValues(self):
        """Set global variables from config"""
        self.config = config.loadConfig()  # Load window manager configuration
        self.mode = self.config["mode"]  # Choose between pure floating mode and an experimental tiling function
        self.processorIdleTime = self.config["procIdle"]  # Time to sleep CPU during event loops

    def configureDispatchers(self):
        """Configure event dispatchers"""
        self.keyboardHandler = keyboard.keyboard(self.config, self)
        self.mouseHandler = mouse.mouse(self.config, self)
        self.doHook = hooks.hooks(self)

        self.borderHandler = border.border(self.config, self)
        self.eventDispatchers = {X.KeyPress: self.keyboardHandler.handleKeypress,        # Keypress
                                 X.KeyRelease: self.keyboardHandler.handleKeyrelease,    # Key release
                                 X.MotionNotify: self.mouseHandler.handleMouseMove,      # Window move
                                 X.ButtonPress: self.mouseHandler.handleMousePress,      # Mouse clicked
                                 X.ButtonRelease: self.mouseHandler.handleMouseRelease,  # Mouse released
                                 X.MapRequest: self.newWindow}                           # New window

    def handleEvents(self):
        """Handle window manager events"""
        pendingEvents = self.display.pending_events()  # Number of pending events
        if pendingEvents:  # If there is an available event in the queue
            event = self.display.next_event()  # Grab it
            if event.type in self.eventDispatchers:  # Check if there is an event dispatcher available for it's type
                self.eventDispatchers[event.type](event)  # If there is, run the handler function with the event as an argument
            else:
                log.log("warning", "Unhandled event: {}".format(str(event)))  # If there isn't, display a warning that an unhandled event has occured. This should be replaced by a logging function
            self.doHook.eventHandled()  # Run hook for event handled
        else:
            sleep(self.processorIdleTime)

    def queryFocused(self):
        """Return the currently focused window"""
        window = self.rootWindow.query_pointer().child  # Get currently focused window
        if window:  # If there is an active window
            return window
        return False

    def stealFocus(self, window):
        """Make a window focused"""
        getattr(window, "set_input_focus")(X.RevertToParent, X.CurrentTime)  # Make window receive input

    def fullscreen(self):
        """Make the current window fullscreen"""
        if self.focusedWindow:
            self.focusedWindow.configure(width=self.displayWidth, height=self.displayHeight, x=0, y=0)
            self.lockedWindows.append(self.focusedWindow)

    def toggleHidden(self):
        """Hide a window e.g. dropdown terminal"""
        if self.hiddenWindow:  # If a window is already hidden
            self.hiddenWindow.map()
            self.hiddenWindow = None
        else:
            if self.focusedWindow:
                self.hiddenWindow = self.focusedWindow
                self.hiddenWindow.unmap()

    def lockWindow(self):
        """Lock a window into a position"""
        if self.focusedWindow:  # We can't lock the root window:
            if self.focusedWindow in self.lockedWindows:  # Check if the window is already locked
                self.lockedWindows.remove(self.focusedWindow) # If the window is already locked, unlock it
            else:
                self.lockedWindows.append(self.focusedWindow)  # Append the currently focused window to the list of locked windows

    def newWindow(self, event):
        """Run for each new window created"""
        window = event.window
        log.log("window", "new window: {}".format(window))
        log.log("window", "window info: {}".format(window.get_attributes()))
        self.mouseHandler.configureMouse(window)  # Receive mouse events for windows
        window.map()  # Map window
        self.windowList.append(window)  # Append window to list of windows
        if window.get_wm_transient_for():
            log.log("window", "{} is a transient window".format(window))
            self.lockedWindows.append(window)
        self.doHook.newWindow()  # Run hook for a created window

    def destroyWindow(self):
        """Destroy the currently focused window"""
        if self.focusedWindow:
            self.focusedWindow.destroy()  # Destroy currently focused window
            self.windowList.remove(self.focusedWindow)  # Remove window from windowList
            log.log("window", "closed window: {}".format(self.focusedWindow))
        self.doHook.destroyWindow()

    def closeDisplay(self):
        log.log("display", "closing display")
        self.display.close()  # Close connection to display server

    def startup(self):
        """Run on startup"""
        if self.config["showloader"] == "true":
            loader.loader()
        wallpaper.setWallpaper(self.config["wallpaper"])  # Set wallpaper
        Popen("xrdb -merge ~/.Xresources", shell=True)  # Load Xresources
        self.doHook.startup()

def main():
    """Executed when program is run as a standalone application"""
    click.secho("Error: ", fg="red", nl=False)
    click.echo("Please use a seperate HackWM wrapper file")
    exit(7)

if __name__ == "__main__":
    main()
